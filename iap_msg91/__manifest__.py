# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Odoo MSG91 SMS Service',
    'category': 'Tools',
    'summary': 'Send sms using MSG91 service provider',
    'description': """
This module links the client sms module and Odoo. It will send the messages for the client and debit its IAP account.
""",
    'depends': ['base_setup', 'iap', 'mail'],
    'data': [
        'data/ir_cron_data.xml',
        'data/sms_account_data.xml',
        'security/ir.model.access.csv',
        'views/sms_account_view.xml',
        'views/sms_trace_view.xml',
        'views/no_credit_template.xml',
        'views/res_config_settings_view.xml'
    ],
    'installable': True,
}
