# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    msg91_auth_key = fields.Char("MSG91 Auth Key", config_parameter='msg91.auth_key')
