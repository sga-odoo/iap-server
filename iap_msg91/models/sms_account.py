# -*- coding: utf-8 -*-
import json
import logging
import math
import phonenumbers
import requests

from datetime import date
from dateutil.relativedelta import relativedelta
from phonenumbers.phonenumberutil import NumberParseException

from odoo import api, fields, models, _
from odoo.exceptions import UserError

API_ENDPOINT = "http://api.msg91.com"

# TODO: Replace this with config params
MONTHLY_LIMIT = 2000
DRAFT_MONTHLY_LIMIT = 100

_logger = logging.getLogger(__name__)


class SmsUser(models.Model):
    _name = "sms.account"
    _inherit = ["mail.thread"]

    name = fields.Char("Company Name")
    code = fields.Char("Prefix Code")
    account_token = fields.Char("IAP Account Token")
    db_uuid = fields.Char("UUID")
    validity_date = fields.Date("Validity Date", track_visibility="onchange")
    sms_monthly_limit = fields.Integer("SMS Limit", default=50)
    sms_monthly_sent = fields.Integer("SMS Sent")
    active = fields.Boolean("Active", default=True)
    state = fields.Selection([("testing", "Testing"), ("production", "Production")], string="Status", required=True, default="testing", track_visibility="onchange")
    sms_type = fields.Selection([('1', "promotional"), ("4", "Transactional")], default="1", required=True)
    msg91_auth_key = fields.Char("MSG91 Auth Key")

    def action_confirm(self):
        self.write({
            "state": "production",
            "sms_monthly_limit": MONTHLY_LIMIT,
            "validity_date": date.today() + relativedelta(months=1)
        })

    def action_draft(self):
        self.write({
            "state": "testing",
            "sms_monthly_limit": DRAFT_MONTHLY_LIMIT,
        })

    def _send_sms(self, numbers, messages):
        """
        Send an SMS to a list of numbers through the API of msg

        :param list(str) numbers: All numbers to which send an SMS
        :param str message: Body of the message

        """
        self.ensure_one()
        res_data, is_ascii = self._count_sms_credit(messages)
        credit = sum(res['credit'] for res in res_data)
        version = self.env.context.get("version")
        if self.sms_monthly_limit < self.sms_monthly_sent + credit:
            if version == "v12":
                raise UserError(_("Your Monthly SMS Limit is over. Contact odoo to increse your limit"))
            _logger.error(_("Your Monthly SMS Limit is over. Contact odoo to increse your limit"))
            return [{
                'res_id': m['res_id'],
                'state': 'insufficient_credit',
                'credit': 0
            } for m in messages]
        msg91_authkey = self.msg91_auth_key or self.env["ir.config_parameter"].sudo().get_param("msg91.auth_key")
        if not msg91_authkey:
            raise UserError(_("Please configure MSG91 account."))
        payload = {
            "sender": self.code,
            "route": self.sms_type,
            "country": "91",
            "sms": [{
                'message': message['content'],
                'to': [message['number']],
            } for message in messages]
        }
        if not is_ascii:
            payload["unicode"] = 1
        headers = {
            "authkey": msg91_authkey,
            "content-type": "application/json"
        }
        sms_url = API_ENDPOINT + "/api/v2/sendsms"

        try:
            res = requests.post(sms_url, data=json.dumps(payload), headers=headers)
            result = res.json()
            if res.status_code == requests.codes.ok:
                if result.get("type") == "success":
                    self.sms_monthly_sent += credit
                    _logger.info('Send SMS: numbers: %r', numbers)
                    response = [{
                        'name': result.get('message'),
                        'res_id': m['res_id'],
                        'credit': m['credit'],
                        'number': m['number'],
                        'state': 'success',
                    } for m in res_data]
                    if version == "v13":
                        self.env['sms.trace'].create(response)
                    return response
            else:
                _logger.error(result['message'])
                if version == "v13":
                    return [{
                        'res_id': m['res_id'],
                        'state': 'server_error',
                        'credit': 0
                    } for m in messages]
        except Exception as e:
            _logger.error(e)
            if version == "v12":
                raise UserError(_("Something went wrong. Please try after some time"))
            return [{
                'res_id': m['res_id'],
                'state': 'server_error',
                'credit': 0
            } for m in messages]

    def _count_sms_credit(self, messages):
        is_ascii = False
        credit = 0
        res_data = []
        for message in messages:
            try:
                message['content'].encode("ascii")
                is_ascii = True
            except UnicodeEncodeError:
                pass
            if is_ascii:
                if len(message['content']) <= 160:
                    credit = 1
                else:
                    credit = math.ceil(len(message['content']) / 153.0)
            else:
                if len(message['content']) <= 70:
                    credit = 1
                else:
                    credit = math.ceil(len(message['content']) / 67.0)
            res_data.append({
                'res_id': message['res_id'],
                'credit': credit,
                'number': message['number']
            })
        return res_data, is_ascii

    def check_balance(self):
        msg91_authkey = self.env["ir.config_parameter"].sudo().get_param("msg91.auth_key")
        sms_url = API_ENDPOINT + "/api/balance.php?type=4&authkey={}".format(msg91_authkey)
        minimum_sms_limit = self.env["ir.config_parameter"].sudo().get_param("msg91.minimum_sms_limit")
        sms_numbers = self.env["ir.config_parameter"].sudo().get_param("msg91.admin_number")
        mobile = []

        try:
            if not msg91_authkey:
                return False
            res = requests.get(sms_url)
            if res.status_code == requests.codes.ok:
                result = res.json()
                if result <= (int(minimum_sms_limit) or 1500):
                    if sms_numbers:
                        for number in sms_numbers.split(","):
                            phone_number = phonenumbers.parse(number, None)
                            mobile.append(phone_number.national_number)
                        message = "Hello,Your message limit is near %s. Please increase to enjoy uninterruptible sms service." % (result)
                        sms_account = self.env['sms.account'].search([('account_token', '=', 'odoo_production_token_2399')])
                        if sms_account:
                            sms_account._send_sms(mobile, message)
        except Exception as e:
            _logger.error(e)

    def update_sms_limit(self):
        sms_accounts = self.env["sms.account"].search([("state", "=", "production"), ("validity_date", "<=", fields.Date.today())])
        for sms_account in sms_accounts:
            sms_account.message_post(_('Total message send last month: %s') % (sms_account.sms_monthly_sent))
            sms_account.write({
                "sms_monthly_sent": 0,
                "validity_date": date.today() + relativedelta(months=1)
            })


class SmsTrace(models.Model):
    _name = "sms.trace"

    _rec_name = 'number'

    name = fields.Char(string="Message ID")
    res_id = fields.Char(string="Record ID")
    number = fields.Char(string="Number")
    credit = fields.Integer("SMS Limit")
    state = fields.Selection([
            ('success', 'Success'),
            ('error', 'Error'),
            ('canceled', 'Canceled')
        ], 'SMS Status', readonly=True, copy=False)
    account_token = fields.Char(string="Account Token")
    fetch_data = fields.Boolean(string="Fetched Data")
    update_data = fields.Boolean(string="Update Data")
