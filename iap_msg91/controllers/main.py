# -*- coding: utf-8 -*-
import json
import logging

from odoo import http
from odoo.http import request
from odoo.exceptions import UserError
from odoo.tools import pycompat
import phonenumbers
from odoo.fields import Datetime
from odoo.addons.iap.models.iap import InsufficientCreditError

DEFAULT_ENDPOINT = ''
IAP_TO_SMS_STATE = {
    '1': 'success',
    '2': 'error',
    '16': 'canceled'
}
_logger = logging.getLogger(__name__)

try:
    from phonenumbers.phonenumberutil import NumberParseException
    _iap_proxy_sms_phonenumbers = True
except ImportError:
    _iap_proxy_sms_phonenumbers = False
    _logger.warning('Phonenumbers library not found. Please install the library from https://pypi.python.org/pypi/phonenumbers')


class SmsController(http.Controller):

    @http.route('/iap/message_send', type='json', auth='none', csrf='false')
    def send_message(self, account_token, message, numbers):
        """
        Send a SMS to a list of numbers through SMS providers

        :param str account_token: IAP account token of the sender
        :param str message: Body of the message
        :param list(str) numbers: All the numbers to send

        All the numbers must be in international format (E.164)
        """
        if not _iap_proxy_sms_phonenumbers:
            raise UserError('The numbers cannot be validated. Please contact Odoo.')
        _logger.info('Attempt to send sms: account_token %s -- numbers: %r', account_token, numbers)
        messages = [{
            'res_id': number,
            'number': number,
            'content': message
        } for number in numbers]
        result = self.send(account_token, messages, False, "v12")
        not_sent = [msg for msg in result if msg['state'] != 'success']
        if not_sent:
            error_message = 'There was an error, message was not sent to these numbers:\n'
            for msg in not_sent:
                error_message += msg['res_id'] + ','
            raise UserError(error_message)
        return True

    @http.route('/iap/sms/1/send', type='json', auth='none', csrf='false')
    def send(self, account_token, messages, raise_error=False, version="v13"):
        sms_account = self._authorize_account(account_token)
        mobile = []
        try:
            for message in messages:
                phone_number = phonenumbers.parse(message['number'], None)
                message['number'] = phone_number.national_number
                mobile.append(phone_number.national_number)
            response = sms_account.with_context(version=version)._send_sms(mobile, messages)
        except NumberParseException:
            raise UserError('The number %s is not correct. It must be in international format (E.164). No SMS were sent.' % message.number)
        return response

    def _authorize_account(self, account_token):
        sms_account = request.env['sms.account'].sudo().search([('account_token', '=', account_token)])
        try:
            if not sms_account:
                raise InsufficientCreditError('{"service_name": "sms"}')
        except InsufficientCreditError as e:
            arguments = json.loads(e.args[0])
            arguments['body'] = pycompat.to_text(request.env['ir.qweb'].render("iap_msg91.no_credit_template"))  # send warning to end user
            e.args = (json.dumps(arguments),)
            raise e
        return sms_account

    @http.route('/iap/create_account', type='http', methods=['POST'], auth='none', csrf=False)
    def create_account(self, *args, **kwargs):
        company_data = json.loads(kwargs['company_info'])
        for company in company_data:
            sms_account = request.env['sms.account'].sudo().search([('account_token', '=', company['account_token'])])
            company_code = company['company_name'].replace(" ", "")
            if len(company_code) > 6:
                prefix_code = company_code[:6].upper()
            else:
                prefix_code = "ODOOIN"
            if not sms_account:
                request.env['sms.account'].with_context(mail_create_nosubscribe=True).sudo().create({
                    'name': company['company_name'],
                    'code': prefix_code,
                    'account_token': company['account_token'],
                    'db_uuid': company['db_uuid']
                })
        return "Success"

    @http.route('/iap/delivery_report', type='http', methods=['POST'], auth='none', csrf=False)
    def delivery_report(self, **kwargs):
        _logger.info("MSG91 Response: %s" % (kwargs))
        for res in json.loads(kwargs['data']):
            for report in res['report']:
                phone_number = phonenumbers.parse(report['number'], 'IN')
                sms = request.env['sms.trace'].sudo().search([('name', '=', res['requestId']),
                                                             ('number', '=', phone_number.national_number), ('update_data', '=', False)])
                if sms:
                    sms.write({
                        'state': IAP_TO_SMS_STATE[report['status']],
                        'update_data': True
                    })
                    if report['status'] in ('2', '16'):
                        sms_account = request.env['sms.account'].sudo().search([('account_token', '=', sms.account_token)])
                        sms_account.write({
                            'credit': sms_account.credit - sms.credit
                        })
        return "Success"

    @http.route('/iap/fetch/delivery_report', type='json', methods=['POST'], auth='none', csrf=False)
    def fetch_delivery_report(self, dbuuid):
        sms_account = request.env['sms.account'].sudo().search([('db_uuid', '=', dbuuid)])
        response = []
        try:
            if not sms_account:
                raise Exception('Unauthorised Access using dbuuid: %s' % dbuuid)
            sms_traces = request.env['sms.trace'].sudo().search([('update_data', '=', True), ('fetch_data', '=', False)])
            response = [{
                            'res_id': m.res_id,
                            'state': m.state,
                        } for m in sms_traces]
            sms_traces.sudo().unlink()
        except Exception as e:
            _logger.error(e)
        return response
